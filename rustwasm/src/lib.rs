//#![no_std]
#![no_main]
#![feature(async_closure)]
use std::{thread, time};
use futures_timer::Delay;
use futures::executor::block_on;

extern crate wee_alloc;

// Use `wee_alloc` as the global allocator.
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;


//use core::panic::PanicInfo;
//use wasi::time;

//#[panic_handler]
//fn panic(_info: &PanicInfo) -> ! {
    //loop {}
//}


//#[cfg(test)]
//mod tests {
    //#[test]
    //fn it_works() {
        //assert_eq!(2 + 2, 4);
    //}
//}

#[repr(C)]
#[repr(packed)]
#[derive(Clone,Copy)]
pub struct RGBW {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub w: u8
}
impl RGBW {
    pub const fn black() -> RGBW {
        RGBW { r:0, g:0, b:0, w:0 }
    }
}

static mut ledstate: [RGBW; 50] = [RGBW::black(); 50];
static mut stateIndex: u8 = 0;

#[no_mangle]
pub extern "C" fn leds() -> *mut [RGBW; 50] {
    let mut i =0u8;
    let brightness = 1u8;
    for elem in unsafe { &mut ledstate[..]} {
        match unsafe {i+stateIndex} % 4 {
            0 => *elem = RGBW { r:brightness, g:0, b:0, w:0 },
            1 => *elem = RGBW { r:0, g:brightness, b:0, w:0 },
            2 => *elem = RGBW { r:0, g:0, b:brightness, w:0 },
            3 => *elem = RGBW { r:0, g:0, b:0, w:brightness },
            _ => *elem = RGBW::black()
        }
        i+=1;
    }
    unsafe {
        stateIndex = stateIndex.wrapping_add(1);
    }

    //println!("Test");

    let dur = time::Duration::from_millis(100);
    //thread::sleep(dur);
    let asyncWait = block_on(Delay::new(dur));
    //let asyncWait = Delay::new(dur);
    //let asyncWait = Delay::new(dur).wait();
    //asyncWait().poll();


    //unsafe {
        //if(ledstate[1] == 0) {
            //unsafe { &mut ledstate }
        //} else {
            //unsafe { 0 as *mut [u8;40] }
        //}
    //}
    unsafe { &mut ledstate }
}
