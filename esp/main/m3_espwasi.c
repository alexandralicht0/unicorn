#include "m3_espwasi.h"
#include "m3/m3_env.h"
#include "m3/m3_api_defs.h"
#include "wasi_core.h"
#include "m3_util.h"
#include "m3/m3_exception.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <esp_timer.h>

typedef uint32_t __wasi_size_t;

#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include <stdio.h>
#include <errno.h>

struct wasi_iovec
{
    __wasi_size_t iov_base;
    __wasi_size_t iov_len;
};

static
__wasi_errno_t errno_to_wasi(int errnum) {
    switch (errnum) {
    case EPERM:   return __WASI_EPERM;   break;
    case ENOENT:  return __WASI_ENOENT;  break;
    case ESRCH:   return __WASI_ESRCH;   break;
    case EINTR:   return __WASI_EINTR;   break;
    case EIO:     return __WASI_EIO;     break;
    case ENXIO:   return __WASI_ENXIO;   break;
    case E2BIG:   return __WASI_E2BIG;   break;
    case ENOEXEC: return __WASI_ENOEXEC; break;
    case EBADF:   return __WASI_EBADF;   break;
    case ECHILD:  return __WASI_ECHILD;  break;
    case EAGAIN:  return __WASI_EAGAIN;  break;
    case ENOMEM:  return __WASI_ENOMEM;  break;
    case EACCES:  return __WASI_EACCES;  break;
    case EFAULT:  return __WASI_EFAULT;  break;
    case EBUSY:   return __WASI_EBUSY;   break;
    case EEXIST:  return __WASI_EEXIST;  break;
    case EXDEV:   return __WASI_EXDEV;   break;
    case ENODEV:  return __WASI_ENODEV;  break;
    case ENOTDIR: return __WASI_ENOTDIR; break;
    case EISDIR:  return __WASI_EISDIR;  break;
    case EINVAL:  return __WASI_EINVAL;  break;
    case ENFILE:  return __WASI_ENFILE;  break;
    case EMFILE:  return __WASI_EMFILE;  break;
    case ENOTTY:  return __WASI_ENOTTY;  break;
    case ETXTBSY: return __WASI_ETXTBSY; break;
    case EFBIG:   return __WASI_EFBIG;   break;
    case ENOSPC:  return __WASI_ENOSPC;  break;
    case ESPIPE:  return __WASI_ESPIPE;  break;
    case EROFS:   return __WASI_EROFS;   break;
    case EMLINK:  return __WASI_EMLINK;  break;
    case EPIPE:   return __WASI_EPIPE;   break;
    case EDOM:    return __WASI_EDOM;    break;
    case ERANGE:  return __WASI_ERANGE;  break;
    default:      return __WASI_EINVAL;
    }
}

m3ApiRawFunction(m3_wasi_unstable_fd_write)
{
    m3ApiReturnType  (uint32_t)
    m3ApiGetArg      (__wasi_fd_t          , fd)
    m3ApiGetArg      (uint32_t             , iovs_offset)
    m3ApiGetArg      (__wasi_size_t        , iovs_len)
    m3ApiGetArgMem   (__wasi_size_t*       , nwritten)

    if (runtime == NULL || nwritten == NULL) { m3ApiReturn(__WASI_EINVAL); }

    ssize_t res = 0;
    struct wasi_iovec *wasi_iov = offset2addr(runtime, iovs_offset);
    for (__wasi_size_t i = 0; i < iovs_len; i++) {
        void* addr = offset2addr(runtime, wasi_iov[i].iov_base);
        size_t len = wasi_iov[i].iov_len;
        if (len == 0) continue;

        printf("fd_write to %i: %.*s\n", fd, len, addr);
        size_t ret = len;
        /*int ret = fwrite (addr, 1, len, 1);*/
        /*if (ret < 0) m3ApiReturn(errno_to_wasi(errno));*/
        res += ret;
        if ((size_t)ret < len) break;
    }
    *nwritten = res;
    m3ApiReturn(__WASI_ESUCCESS);
}

m3ApiRawFunction(m3_wasi_poll_oneoff)
{
    m3ApiReturnType  (uint32_t)
    /*m3ApiGetArgMem   (const __wasi_subscription_t*, subscriptions)*/
    m3ApiGetArg      (__wasi_size_t, subscriptions_offset)
    m3ApiGetArgMem   (__wasi_event_t*       , out_event)
    m3ApiGetArg      (__wasi_size_t        , num_subscriptions)
    m3ApiGetArgMem   (__wasi_size_t*       , nevents)

    printf("Poll_oneoff\n");

    //todo: getargmem should be fine
    struct __wasi_subscription_t *subscriptions = offset2addr(runtime, subscriptions_offset);

    /*printf("poll_oneoff2: %p %p %d\n", subscriptions, out_event, num_subscriptions);*/
    if (subscriptions == NULL || out_event == NULL) { m3ApiReturn(__WASI_EINVAL); }

    __wasi_subscription_t* effective_sub = NULL;
    for(__wasi_size_t i = 0; i < num_subscriptions; i++) {
      __wasi_subscription_t* sub = &subscriptions[i];
      if(sub->type != __WASI_EVENTTYPE_CLOCK) {
        printf("Ignoring unsupported wasi event type\n");
        continue;
      }
      /*struct __wasi_subscription_u_clock_t* sub_as_clock = &sub->u.clock;*/
      if(sub->u.clock.clock_id == __WASI_CLOCK_REALTIME) {
        printf("Ignoring unsupported realtime clock\n");
        continue;
      }
      if(sub->u.clock.flags != 0) {
        printf("Ignoring unsupported clock flags\n");
      }
      if(!effective_sub || (effective_sub && effective_sub->u.clock.timeout > sub->u.clock.timeout)) {
        effective_sub = sub;
      }
    }
    if(effective_sub) {
      // TODO: nop sleep for all < 1 ms?
      uint64_t ms = effective_sub->u.clock.timeout/1000000;
      vTaskDelay(ms/portTICK_PERIOD_MS);
      out_event->userdata = effective_sub->userdata;
      out_event->error = 0;
      out_event->type = __WASI_EVENTTYPE_CLOCK;

      *nevents = 1;
    } else {
      printf("No effective sub!\n");
      out_event->error = __WASI_EINVAL;
    }

    m3ApiReturn(__WASI_ESUCCESS);
}

m3ApiRawFunction(m3_wasi_clock_time_get)
{
    m3ApiReturnType  (__wasi_timestamp_t)
    m3ApiGetArg      (__wasi_clockid_t   , clock_id)
    m3ApiGetArg      (__wasi_timestamp_t , _precision)
    m3ApiGetArgMem   (__wasi_timestamp_t*, out_time)

    if (out_time == NULL) { m3ApiReturn(__WASI_EINVAL); }
    //TODO: different clock support
    if (clock_id != __WASI_CLOCK_MONOTONIC) { m3ApiReturn(__WASI_EINVAL); }
    int64_t time_microseconds = esp_timer_get_time();
    *out_time = time_microseconds * 1000;
    printf("Out time: %" PRIu64 "\n", *out_time);

    m3ApiReturn(__WASI_ESUCCESS);
}



static
M3Result SuppressLookupFailure(M3Result i_result)
{
    if (i_result == c_m3Err_functionLookupFailed)
        return c_m3Err_none;
    else
        return i_result;
}


M3Result  m3_LinkESPWASI  (IM3Module module)
{
    M3Result result = c_m3Err_none;

    const char* ns  = "wasi_snapshot_preview1";

// Untested:
_   (SuppressLookupFailure (m3_LinkRawFunction (module, ns, "fd_write",                &m3_wasi_unstable_fd_write)));
_   (SuppressLookupFailure (m3_LinkRawFunction (module, ns, "poll_oneoff",             &m3_wasi_poll_oneoff)));
_   (SuppressLookupFailure (m3_LinkRawFunction (module, ns, "clock_time_get",          &m3_wasi_clock_time_get)));

_catch:
    return result;
}
